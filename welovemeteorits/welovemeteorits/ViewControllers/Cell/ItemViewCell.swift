//
//  ItemViewCell.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 26/07/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import UIKit

class ItemViewCell: UITableViewCell {
    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
