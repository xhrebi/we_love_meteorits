//
//  MeteoritDetailViewController.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 26/07/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import UIKit
import MapKit

class MeteoritDetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var DetailView: UIView!
    @IBOutlet weak var meteoritNameLabel: UILabel!
    @IBOutlet weak var meteoritMassLabel: UILabel!
    @IBOutlet weak var meteoritYearLabel: UILabel!
    @IBOutlet weak var meteoritLocationLabel: UILabel!
    @IBOutlet weak var anyMapImg: UIImageView!
    @IBOutlet weak var anyMapLabel: UILabel!
    @IBOutlet weak var placeholderImg: UIImageView!
    
    var regionRadius: CLLocationDistance = 4000000
    var meteoritName: String!
    var meteoritMass: String!
    var meteoritYear: String!
    var meteoritlatitude: String!
    var meteoritlongitude: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        title = "Detail"
        
        anyMapImg.isHidden = true
        anyMapLabel.isHidden = true
        
        meteoritNameLabel.text = meteoritName       
        meteoritMassLabel.text = "\(meteoritMass!) g"
        meteoritYearLabel.text = String(meteoritYear.prefix(4))
        
        if let latitude = Double(meteoritlatitude!), let longitude = Double(meteoritlongitude!) {
            checkGeoLocation(latitude: latitude, longitude: longitude)
            
            let initialLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            centerMapOnLocation(location: initialLocation)
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = initialLocation
            annotation.title = self.meteoritName
            mapView.addAnnotation(annotation)
        }
    }
    
    func checkGeoLocation(latitude: Double, longitude: Double) {
        if (longitude == 0 || latitude == 0) {
            meteoritLocationLabel.isHidden = true
            placeholderImg.isHidden = true
            mapView.isHidden = true
            
            anyMapImg.isHidden = false
            anyMapLabel.isHidden = false
            
            return
        }
        meteoritLocationLabel.text = "\(latitude), \(longitude)"
    }
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegion(center: location,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
