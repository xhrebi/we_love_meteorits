//
//  MainViewController.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 26/07/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var meteoritsCountLabel: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    var refreshControl = UIRefreshControl()
    
    var meteorits: Results<Meteorit>?
    var filtered: Results<Meteorit>?
    var filterring = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Meteorits"
        lastUpdateLabel.isHidden = true
        meteoritsCountLabel.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Meteorits Data ...")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        let logo = UIImage(named: "asteroid.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        search.searchBar.placeholder = "Search By Name"
        self.navigationItem.searchController = search
        
        fetchMeteoritsData(url: "https://data.nasa.gov/resource/gh4g-9sfh.json?$where=year%3E='2011'&$order=mass%20DESC")
    }
    
    @objc func refresh(sender: AnyObject) {
        self.fetchMeteoritsData(url: "https://data.nasa.gov/resource/gh4g-9sfh.json?$where=year%3E='2011'&$order=mass%20DESC", refresh: true)
        refreshControl.endRefreshing()
    }
    
    func isConnected() -> Bool {
        let status = Reach().connectionStatus()
        
        switch status {
        case .unknown, .offline:
            return false
        case .online(_):
            return true
        }
    }
    
    func showNoInternetConnAlert() {
        let alert = UIAlertController(title: "Warning", message: "You are not connected to the Internet. Connected to the Internet and try again.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func fetchMeteoritsData(url: String, refresh: Bool = false) {
        self.meteorits = DBManager.sharedInstance.getMeteorits()
        
        // Any meteorit data in mobile device, need to fetch from API or request to refresh data
        if (self.meteorits?.count == 0 || refresh) {
            if (isConnected()) {
                self.meteoritsCountLabel.isHidden = false
                self.meteoritsCountLabel.text = "Loading ..."
                guard let url = URL(string: url) else { return }
               
                // fetch data from API
                URLSession.shared.dataTask(with: url) {(data, response, error) in
                    guard let data = data else { return }
                    DispatchQueue.main.async {
                        do {
                            let meteorits = try JSONDecoder().decode([Meteorit].self, from: data)
                            
                            if (refresh) {
                                // delete current data from DB
                                DBManager.sharedInstance.deleteAllFromDB()
                            }
                            
                            for meteorit in meteorits {
                                DBManager.sharedInstance.saveMeteorit(meteorit)
                            }
                            
                            DBManager.sharedInstance.saveLastUpdateDateTime()
                            
                            self.meteorits = DBManager.sharedInstance.getMeteorits()
                            
                            self.lastUpdateLabel.isHidden = false
                            let mydf = MyDateFormatter()
                            self.lastUpdateLabel.text = "Last Update: \(mydf.getDateAsString(DBManager.sharedInstance.getLastUpdateDateTime()))"
                            self.tableView.reloadData()
                            
                        } catch let parsingError {
                            print("Error At Parsing JSON File!", parsingError)
                        }
                    }
                }.resume()
            } else {
                self.showNoInternetConnAlert()
            }
            return
        }
        
        self.lastUpdateLabel.isHidden = false
        let mydf = MyDateFormatter()
        self.lastUpdateLabel.text = "Last Update: \(mydf.getDateAsString(DBManager.sharedInstance.getLastUpdateDateTime()))"
        
        self.tableView.reloadData()
    }
    
    func goToMeteoritDetailPage(meteorit: Meteorit) {
        let meteoritDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MeteoritDetailViewController") as! MeteoritDetailViewController
        meteoritDetailVC.meteoritName = meteorit.name
        meteoritDetailVC.meteoritMass = meteorit.mass
        meteoritDetailVC.meteoritYear = meteorit.year
        meteoritDetailVC.meteoritlatitude = meteorit.reclat
        meteoritDetailVC.meteoritlongitude = meteorit.reclong
        self.navigationController?.pushViewController(meteoritDetailVC, animated: true)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (filterring ? filtered?.count : meteorits?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemViewCell") as! ItemViewCell
        
        if (filterring) {
            if let filtered = filtered {
                cell.itemLabel.text = filtered[indexPath.row].name
                cell.massLabel.text = "\(filtered[indexPath.row].mass) g"
            }
        } else {
            if let meteorits = meteorits {
                cell.itemLabel.text = meteorits[indexPath.row].name
                cell.massLabel.text = "\(meteorits[indexPath.row].mass) g"
            }
        }
        
        meteoritsCountLabel.isHidden = false
        
        if let meteorits = meteorits {
            meteoritsCountLabel.text = "Meteorits: \(meteorits.count)"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (filterring) {
            if let filtered = filtered {
                goToMeteoritDetailPage(meteorit: filtered[indexPath.row])
            }
        } else {
            if let meteorits = meteorits {
                goToMeteoritDetailPage(meteorit: meteorits[indexPath.row])
            }
        }
    }
}

extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, !text.isEmpty {
            self.filtered = self.meteorits?.filter("name CONTAINS[c] %@", text.lowercased())
            self.filterring = true
        }
        else {
            filterring = false
            filtered = nil
        }
        tableView.reloadData()
    }
}
