//
//  DBManager.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 13/08/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    private var database: Realm
    static var sharedInstance = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func getMeteorits() -> Results<Meteorit> {
        let meteorits: Results<Meteorit> = database.objects(Meteorit.self)
        return meteorits
    }
    
    func saveMeteorit(_ meteorit: Meteorit) {
        try! database.write {
            database.add(meteorit)
        }
    }
    
    func getLastUpdateDateTime() -> Date {
        let date: Date = database.objects(LastUpdateInfo.self).first?.date ?? Date()
        return date        
    }
    
    func saveLastUpdateDateTime() {
        let currDateTime = LastUpdateInfo()
        try! database.write {
            database.add(currDateTime)
        }
    }
    
    func deleteAllFromDB() {
        try! database.write {
            database.deleteAll()
        }
    }
}
