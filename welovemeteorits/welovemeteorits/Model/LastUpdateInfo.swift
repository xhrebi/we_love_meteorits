//
//  LastUpdateInfo.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 13/08/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import Foundation
import RealmSwift

class LastUpdateInfo: Object {
    @objc dynamic var date = Date()
}
