//
//  Meteorit.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 27/07/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import Foundation
import RealmSwift

class Meteorit: Object, Decodable {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var mass: String = ""
    @objc dynamic var year: String = ""
    @objc dynamic var reclat: String = ""
    @objc dynamic var reclong: String = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case mass
        case year
        case reclat
        case reclong
    }
    
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.id = try container.decode(String.self, forKey: .id)
        self.mass = try container.decode(String.self, forKey: .mass)
        self.year = try container.decode(String.self, forKey: .year)
        self.reclat = try container.decode(String.self, forKey: .reclat)
        self.reclong = try container.decode(String.self, forKey: .reclong)
    }
}
