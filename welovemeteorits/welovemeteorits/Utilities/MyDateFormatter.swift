//
//  MyDateFormatter.swift
//  welovemeteorits
//
//  Created by Pavel Hřebíček on 13/08/2019.
//  Copyright © 2019 Pavel Hřebíček. All rights reserved.
//

import Foundation

class MyDateFormatter {
    private var dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    func getDateAsString(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.string(from: date)
    }
}
